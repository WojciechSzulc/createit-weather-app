<?php

namespace App\Services\Geocoding;

use Arr;
use Cache;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use JetBrains\PhpStorm\Pure;
use Log;
use Throwable;

class GeocodingApi implements GeocodingApiInterface
{
    protected array $config;

    public function __construct()
    {
        $this->config = config('api')['geocoding'];
    }

    private function getCacheKey(string $query): string
    {
        return 'geocoding-service_' . $query;
    }

    /**
     * @throws Throwable
     */
    private function getData($url, $query): Response
    {
        try {
            return Http::get($url, $query);
        } catch (Throwable $throwable) {
            Log::error($throwable->getMessage());

            throw $throwable;
        }
    }

    /**
     * @throws Throwable
     */
    public function getGeocodeForQuery(string $query): array
    {
        $query = $this->buildQuery([$this->config['parameter'] => $query]);

        $response = Cache::get($this->getCacheKey($query));

        if ($response === null) {
            try {
                $apiResponse = $this->getData($this->getUrl(), $query);
                $response = $apiResponse->object()[0];
                Cache::put($this->getCacheKey($query), $response, now()->addMinutes(60));
            } catch (Throwable) {
                $message = "Location not found for $query !";
                Log::error($message);

                throw new GeocodingNotFoundException($message);
            }
        }

        return [
            $response->lat,
            $response->lon,
            $response->name,
            $response->state ?? null,
            $response->country,
        ];
    }

    #[Pure] protected function buildQuery(array $params = []): string
    {
        $params['appid'] = $this->config['key'];

        return Arr::query($params);
    }

    private function getUrl(): string
    {
        return $this->config['url'];
    }
}
