<?php

namespace App\Services\Geocoding;

interface GeocodingApiInterface
{
    public function getGeocodeForQuery(string $query): array;
}
