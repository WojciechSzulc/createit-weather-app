<?php

namespace App\Services\Geocoding;

use Throwable;

abstract class GeocodingService
{
    /**
     * @throws Throwable
     */
    public static function getGeocodeForCity(string $query): array
    {
        $api = new GeocodingApi();

        return $api->getGeocodeForQuery($query);
    }
}
