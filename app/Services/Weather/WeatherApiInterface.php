<?php

namespace App\Services\Weather;

interface WeatherApiInterface
{
    public function getWeatherForGeo(float $lat, float $long): WeatherResponse;
}
