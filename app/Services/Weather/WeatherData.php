<?php

namespace App\Services\Weather;

class WeatherData
{
    public float $latitude;
    public float $longitude;
    public string $city;
    public string $state;
    public string $country;
    public float $averageTemperature;
    public array $results;

    public function __construct($latitude, $longitude, $city, $state, $country, $averageTemperature, $results)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->averageTemperature = $averageTemperature;
        $this->results = $results;
    }
}
