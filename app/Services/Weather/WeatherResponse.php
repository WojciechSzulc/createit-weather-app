<?php

namespace App\Services\Weather;

class WeatherResponse
{
    public mixed $originalResponse;

    public string $provider;
    public int $status;
    public string $message;
    public float $temperature;

    public function __construct($provider = null, $status = null, $message = null, $temperature = null)
    {
        $this->provider = $provider;
        $this->status = $status;
        $this->message = $message;
        $this->temperature = $temperature;
    }
}
