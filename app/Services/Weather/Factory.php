<?php

namespace App\Services\Weather;

use Log;
use Throwable;

final class Factory
{
    /**
     * @throws WeatherServiceException
     */
    public static function factory(string $api): WeatherApiInterface
    {
        $api_config = config('api')['weather'][$api];
        $class = $api_config['class'];

        try {
            $weatherInstance = new $class();
        } catch (Throwable $throwable) {
            $message = "Class for $api could not be initialized! {$throwable->getMessage()}";
            Log::debug($throwable->getMessage());
            Log::error($message);

            throw new WeatherServiceException($message);
        }

        if ($weatherInstance instanceof WeatherApiInterface === false) {
            $message = "Class for $api api does not implement WeatherApiInterface!";
            Log::error($message);

            throw new WeatherServiceException($message);
        }

        return $weatherInstance;
    }
}
