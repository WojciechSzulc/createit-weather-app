<?php

namespace App\Services\Weather;

use Arr;
use Cache;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use JetBrains\PhpStorm\Pure;
use Log;
use Throwable;

abstract class BaseWeatherApi implements WeatherApiInterface
{
    protected string $api;
    protected array $config;
    protected string $key_param_name = 'key';
    protected string $latitude_param_name = 'lat';
    protected string $longitude_param_name = 'lon';

    public function __construct()
    {
        $this->config = config('api')['weather'][$this->api];
    }

    private function getCacheKey(string $query): string
    {
        return 'weather-service_' . $this->api . '_' . $query;
    }

    private function getData($url, $query): Response
    {
        return Http::get($url, $query);
    }

    /**
     * @throws WeatherServiceException
     */
    public function getWeatherForGeo(float $lat, float $long): WeatherResponse
    {
        $query = $this->buildQuery([$this->latitude_param_name => $lat, $this->longitude_param_name => $long]);

        return $this->getResponse($query);
    }

    #[Pure] protected function buildQuery(array $params = []): string
    {
        $key_required = $this->config['key_required'];
        if ($key_required === true) {
            $params[$this->key_param_name] = $this->config['key'];
        }

        $default_params = $this->config['default_params'] ?? null;
        if (!empty($default_params) && is_array($default_params)) {
            $params = array_merge($default_params, $params);
        }

        return Arr::query($params);
    }

    private function getUrl(): string
    {
        return $this->config['url'];
    }

    /**
     * @throws WeatherServiceException
     */
    protected function getResponse(string $query): WeatherResponse
    {
        $apiResponse = Cache::get($this->getCacheKey($query));

        if ($apiResponse === null) {
            try {
                $apiResponse = $this->getData($this->getUrl(), $query);
                $apiResponse = $this->parseResponse($apiResponse);
                Cache::put($this->getCacheKey($query), $apiResponse, now()->addMinute());
            } catch (Throwable $throwable) {
                $message = __CLASS__ . ": Couldn't retrieve and/or parse weather data from $this->api API";
                Log::debug($throwable->getMessage());
                Log::error($message);

                throw new WeatherServiceException($message);
            }
        }

        return $apiResponse;
    }

    protected function parseResponse(Response $apiResponse): WeatherResponse
    {
        $response = new WeatherResponse();
        $response->originalResponse = $apiResponse->object();
        $response->status = $apiResponse->status();
        $response->provider = $this->config['name'];

        if ($response->status === 200) {
            $this->parseTemperature($response);
        } else {
            $this->parseMessage($response);
        }

        return $response;
    }

    protected function parseTemperature(WeatherResponse $response)
    {
    }

    protected function parseMessage(WeatherResponse $response)
    {
    }
}
