<?php

/**
 * Open-Meteo weather forecast API
 * Website: https://open-meteo.com/en/docs
 *
 * No API key required
 */

namespace App\Services\Weather\Providers;

use App\Services\Weather\BaseWeatherApi;
use App\Services\Weather\WeatherApiInterface;
use App\Services\Weather\WeatherResponse;

class OpenMeteo extends BaseWeatherApi implements WeatherApiInterface
{
    protected string $api = 'open_meteo';
    protected string $latitude_param_name = 'latitude';
    protected string $longitude_param_name = 'longitude';

    protected function parseTemperature(WeatherResponse $response)
    {
        $response->temperature = $response->originalResponse->current_weather->temperature;
    }

    protected function parseMessage(WeatherResponse $response)
    {
        $response->message = $response->originalResponse->reason;
    }
}
