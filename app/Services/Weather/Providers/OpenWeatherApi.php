<?php

/**
 * OpenWeather weather forecast API
 * Website: https://openweathermap.org/api
 *
 * API key is required, free plan available
 */

namespace App\Services\Weather\Providers;


use App\Services\Weather\BaseWeatherApi;
use App\Services\Weather\WeatherApiInterface;
use App\Services\Weather\WeatherResponse;

class OpenWeatherApi extends BaseWeatherApi implements WeatherApiInterface
{
    protected string $api = 'openweathermap';
    protected string $key_param_name = 'appid';

    protected function parseTemperature(WeatherResponse $response)
    {
        $response->temperature = $response->originalResponse->main->temp;
    }

    protected function parseMessage(WeatherResponse $response)
    {
        $response->message = $response->originalResponse->message;
    }
}
