<?php

/**
 * Weather API
 * Website: https://www.weatherapi.com/api-explorer.aspx
 *
 * API key is required, free plan available
 */

namespace App\Services\Weather\Providers;

use App\Services\Weather\BaseWeatherApi;
use App\Services\Weather\WeatherApiInterface;
use App\Services\Weather\WeatherResponse;

class WeatherApi extends BaseWeatherApi implements WeatherApiInterface
{
    protected string $api = 'weatherapi';
    protected string $key_param_name = 'key';

    public function getWeatherForGeo(float $lat, float $long): WeatherResponse
    {
        $query = $this->buildQuery(['q' => "$lat,$long"]);

        return $this->getResponse($query);
    }

    protected function parseTemperature(WeatherResponse $response)
    {
        $response->temperature = $response->originalResponse->current->temp_c;
    }

    protected function parseMessage(WeatherResponse $response)
    {
        $response->message = $response->originalResponse->error->message;
    }
}
