<?php

/**
 * Weatherbit weather forecast API
 * Website: https://www.weatherbit.io/api/weather-current
 *
 * API key is required, free plan available
 */

namespace App\Services\Weather\Providers;


use App\Services\Weather\BaseWeatherApi;
use App\Services\Weather\WeatherApiInterface;
use App\Services\Weather\WeatherResponse;

class WeatherBit extends BaseWeatherApi implements WeatherApiInterface
{
    protected string $api = 'weatherbit';

    protected function parseTemperature(WeatherResponse $response)
    {
        $response->temperature = $response->originalResponse->data[0]->temp;
    }

    protected function parseMessage(WeatherResponse $response)
    {
        $response->message = $response->originalResponse->error;
    }
}
