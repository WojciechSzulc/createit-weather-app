<?php

namespace App\Services\Weather;

use App\Services\Geocoding\GeocodingService;
use Throwable;

abstract class WeatherService
{
    /**
     * @throws Throwable
     * @throws WeatherServiceException
     */
    public static function getWeatherDataForCityQuery(string $query): WeatherData
    {
        [
            $lat,
            $lon,
            $city,
            $state,
            $country,
        ] = GeocodingService::getGeocodeForCity($query);

        $providers = config('api')['weather'];
        $results = [];
        $i = 0;
        $sum = 0;

        foreach ($providers as $provider => $array) {
            $result = Factory::factory($provider)->getWeatherForGeo(lat: $lat, long: $lon);
            $results[] = $result;

            if ($result->status === 200) {
                $sum += $result->temperature;
                $i++;
            }
        }

        $averageTemperature = number_format($sum / $i, 1);

        return new WeatherData(
            $lat,
            $lon,
            $city,
            $state,
            $country,
            $averageTemperature,
            $results
        );
    }
}
