<?php

namespace App\Http\Repositories;

use App\Models\WeatherResult;
use App\Models\WeatherResultDetail;
use App\Services\Weather\WeatherData;
use App\Services\Weather\WeatherResponse;

class WeatherRepository implements WeatherRepositoryInterface
{
    public function saveResults(WeatherData $results)
    {
        $weatherResult = new WeatherResult();
        $weatherResult->latitude = $results->latitude;
        $weatherResult->longitude = $results->longitude;
        $weatherResult->city = $results->city;
        $weatherResult->state = $results->state;
        $weatherResult->country = $results->country;
        $weatherResult->average_temperature = $results->averageTemperature;

        $weatherResult->save();

        foreach ($results->results as $result) {
            if (!$result instanceof WeatherResponse) continue;

            $weatherResultDetail = new WeatherResultDetail();
            $weatherResultDetail->provider = $result->provider;
            $weatherResultDetail->status = $result->status;
            $weatherResultDetail->message = $result->message;
            $weatherResultDetail->temperature = $result->temperature;

            $weatherResult->details()->save($weatherResultDetail);
        }
    }
}
