<?php

namespace App\Http\Repositories;

use App\Services\Weather\WeatherData;

interface WeatherRepositoryInterface
{
    public function saveResults(WeatherData $results);
}
