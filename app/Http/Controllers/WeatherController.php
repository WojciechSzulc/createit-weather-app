<?php

namespace App\Http\Controllers;

use App\Http\Repositories\WeatherRepositoryInterface;
use App\Services\Geocoding\GeocodingNotFoundException;
use App\Services\Weather\WeatherService;
use App\Services\Weather\WeatherServiceException;
use Cache;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Log;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;

class WeatherController extends Controller
{
    private WeatherRepositoryInterface $weatherRepository;
    private string $sessionCacheKey;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(WeatherRepositoryInterface $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
        $this->sessionCacheKey = 'weather_results_' . session()->get('_token');
    }

    public function index(): View
    {
        $weather_results = Cache::get($this->sessionCacheKey);

        return view('index', compact('weather_results'));
    }

    public function submit(Request $request): RedirectResponse
    {
        $city = $request->post('city');
        $state = $request->post('state');
        $country = $request->post('country');

        $query = "$city,$state,$country";
        try {
            $results = WeatherService::getWeatherDataForCityQuery($query);
        } catch (GeocodingNotFoundException) {
            return redirect()->route('index')->with(['error' => 'Location not found!']);
        } catch (WeatherServiceException) {
            return redirect()->route('index')->with(['error' => 'Weather API caused an error!']);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return redirect()->route('index')->with(['error' => 'An error has occurred!']);
        }

        $this->weatherRepository->saveResults($results);

        Cache::put($this->sessionCacheKey, $results, now()->addMinutes(10));

        return redirect()->route('index');
    }
}
