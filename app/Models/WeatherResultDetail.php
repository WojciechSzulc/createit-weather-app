<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WeatherResultDetail extends Model
{
    public function weatherResult(): BelongsTo
    {
        return $this->belongsTo(WeatherResult::class);
    }
}
