# Weather App

*This is project created by Wojciech Szulc for createIT for recruitment purposes only.*

I'm using [Laravel Sail](https://laravel.com/docs/8.x/sail) with PHP 8.1 and [Laravel 8.82.0](https://laravel.com/docs/8.x).

It requires [Docker](https://www.docker.com/) for Linux or [Docker Desktop](https://www.docker.com/products/docker-desktop) for Windows.

If you want to run Sail on Windows, you need WSL2 and Ubuntu environment already configured as well, more info in the [documentation](https://laravel.com/docs/8.x#getting-started-on-windows).  
On top of that all `sail` commands must be run from within the WSL environment (where project should reside).

## Before first launch:

Before launching Sail for the first time, you need to:
* duplicate the `.env.example` and change copy's name to `.env`
* change any env variables you wish to change
  * OpenWeatherMap API Key **is required**

## Launching app

I used Laravel Sail to run the development environment:
```shell
./vendor/bin/sail up -d
```
To close and remove containers use:
```shell
./vendor/bin/sail down
```

## After first launch

Run following commands:

```shell
sail artisan key:generate --ansi
sail artisan migrate
```

# Accessing the website

Website should be accessible under the http://localhost url.

You can add `127.0.0.1 create-it.test` entry to your `hosts` file and then access this website under http://create-it.test/ as well.

Laravel Sail does not support SSL. 


# API Providers

This project supports 4 weather API providers out of the box:

* [OpenWeather](https://openweathermap.org/) - requires registration and API key, free plan available
  * Also used for geocoding, hence is **mandatory** for site to work
* [Open-Meteo](https://open-meteo.com/) - doesn't require any registratior or API key
* [Weatherbit](https://www.weatherbit.io/) - requires registration and API key, free plan available
  * Optional 
* [Weather API](https://www.weatherapi.com/) - requires registration and API key, free plan available
  * Optional 

## Adding new API provider:

1. Add new entry under `weather` group in `config/api.php` file
   * The only required field is `class`
   * If you plan to expand on `App\Services\Weather\BaseWeatherApi` then `name`, `key_required` and `url` are also required. 
2. Create new class under `App\Services\Weather\Providers`. This class *MUST* implement `App\Services\Weather\WeatherApiInterface` interface
   * This class can expand on `App\Services\Weather\BaseWeatherApi` if it can be easily applied
     * If you expand on `BaseWeatherApi`, make sure to fill following class properties:
       * `api` — needs to match name of new group added to `api.php` config file
       * `key_param_name` — (optional) if API requires passing API Key, this property should hold the name of key's query parameter
     * Depending on API you might need to change `getWeatherForGeo()` method, or simply change following properties:
       * `latitude_param_name` — (optional) this property should hold the name of latitude's query parameter
       * `longitude_param_name` — (optional) this property should hold the name of longitude's query parameter
     * Make sure to override the `parseTemperature()` method to populate `WeatherResponse` object with temperature
   * If you do not plan on expanding the `BaseWeatherApi`, you should implement entire flow from accepting Latitude and Longitude, through connecting to external API, to returning the populated `WeatherResponse` object.  
3. Add full name of class from point 2. to the config entry you added in point 1.  
