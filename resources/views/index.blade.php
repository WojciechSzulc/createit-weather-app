<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Weather App</title>
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-dark bg-opacity-75">

<main>
    <div class="container my-5">
        @if(session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif

        <div class="bg-white row p-4 pb-0 pe-lg-0 pt-lg-5 align-items-center rounded-3 border shadow-lg">
            <div class="col-md-8 p-3 p-lg-5 pt-lg-3">
                <h1 class="display-4 fw-bold lh-1">Current Weather Checker</h1>
                <p class="lead">Provide your city, state and country to see current weather.</p>

                <form class="mb-4 mb-lg-3" method="post">
                    @csrf
                    <div class="input-group mb-1">
                        <span class="input-group-text required">City</span>
                        <input type="text" id="city" name="city" class="form-control" placeholder="ex. Poznań" value="{{ $weather_results->city ?? null }}" required/>
                    </div>
                    <div class="input-group mb-1">
                        <span class="input-group-text">State</span>
                        <input type="text" id="state" name="state" class="form-control" placeholder="ex. lubelskie" value="{{ $weather_results->state ?? null }}"/>
                    </div>
                    <div class="input-group mb-1">
                        <span class="input-group-text">Country Code</span>
                        <input type="text" id="country" name="country" class="form-control" placeholder="ex. PL" value="{{ $weather_results->country ?? null }}"/>
                    </div>

                    <button type="submit" class="btn btn-primary btn-lg px-4 me-md-2 fw-bold">@if($weather_results === null) Submit @else Refresh @endif</button>
                </form>
            </div>
            <div class="col-md-4 p-3 p-lg-5 pt-lg-3 text-center">
                @if ($weather_results !== null)
                <div class="display-6 fw-bold lh-1">
                    <div>{{ $weather_results->city }}</div>
                    <div class="small fs-4 fst-italic">{{ $weather_results->state }}, {{ $weather_results->country }}</div>
                </div>
                <div class="display-4 fw-bold lh-1 mt-3">{{ $weather_results->averageTemperature }}&deg;C</div>
                <div class="d-inline-block mt-4 details-trigger">
                  <button class="btn btn-primary btn-sm" type="button" disabled>See details</button>
                    <div class="details-container bg-light shadow rounded-2 p-2">
                        @foreach($weather_results->results as $details)
                        <div class="row">
                            <span class="col-sm-8 fw-bold">{{ $details->provider }}</span>
                            <span class="col-sm-4 text-end">{{ $details->temperature ?? 'N/A' }}&deg;C</span>
                        </div>
                        @endforeach
                    </div>
                </div>
                @else
                <div class="fw-bold fst-italic lh-1">Please provide location to see the weather.</div>
                @endif
            </div>
        </div>
    </div>

    <div class="b-example-divider"></div>
</main>

<div class="bg-dark text-secondary px-4 py-5 text-center footer">
    Copyright &copy; 2022 Wojciech Szulc
</div>

</body>
</html>
