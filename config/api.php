<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Geocoding APIs
    |--------------------------------------------------------------------------
    |
    |
    */

    'geocoding' => [
        'url' => 'https://api.openweathermap.org/geo/1.0/direct',
        'parameter' => 'q',
        'key' => env('OPENWEATHERMAP_GEOCODING_API_KEY'),
    ],


    /*
    |--------------------------------------------------------------------------
    | Weather APIs
    |--------------------------------------------------------------------------
    |
    |
    */

    'weather' => [
        'openweathermap' => [
            'name' => 'OpenWeather',
            'url' => 'https://api.openweathermap.org/data/2.5/weather',
            'class' => \App\Services\Weather\Providers\OpenWeatherApi::class,
            'key_required' => true,
            'key' => env('OPENWEATHERMAP_API_KEY'),
            'default_params' => [
                'units' => 'metric'
            ]
        ],
        'weatherapi' => [
            'name' => 'Weather API',
            'url' => 'https://api.weatherapi.com/v1/current.json',
            'class' => \App\Services\Weather\Providers\WeatherApi::class,
            'key_required' => true,
            'key' => env('WEATHERAPI_API_KEY')
        ],
        'weatherbit' => [
            'name' => 'Weatherbit',
            'url' => 'https://api.weatherbit.io/v2.0/current',
            'class' => \App\Services\Weather\Providers\WeatherBit::class,
            'key_required' => true,
            'key' => env('WEATHERBIT_API_KEY')
        ],
        'open_meteo' => [
            'name' => 'Open-Meteo',
            'url' => 'https://api.open-meteo.com/v1/forecast',
            'class' => \App\Services\Weather\Providers\OpenMeteo::class,
            'key_required' => false,
            'default_params' => [
                'current_weather' => 'true'
            ]
        ],
    ],
];
