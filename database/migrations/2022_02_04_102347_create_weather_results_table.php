<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_results', function (Blueprint $table) {
            $table->id();

            $table->float('latitude');
            $table->float('longitude');
            $table->string('city');
            $table->string('state')->nullable();
            $table->string('country');
            $table->float('average_temperature')->nullable();

            $table->timestamps();

            $table->index(['city', 'state', 'country']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_results');
    }
}
