<?php

use App\Models\WeatherResult;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherResultDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_result_details', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(WeatherResult::class)->constrained();
            $table->string('provider');
            $table->unsignedSmallInteger('status');
            $table->text('message')->nullable();
            $table->float('temperature')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_result_details');
    }
}
